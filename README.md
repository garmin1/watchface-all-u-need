AllUNeed - Digital Watch Face with Weather

A Watch Face for the Garmin Connect IQ platform.

This is my first Watch Face! Primarily I developed it for myself because I was 
interested in the way this works (so it should be called "AllINeed...").  
So there is a lack of supported devices for now.
Let's see if there comes any feedback... 

Features:
- Weekday (English and German)
- Date
- Time with 12 or 24 hour support
- Smartphone Notifications with number
- Phone connected status
- Current battery level
- Alarm set status
- Sunrise/sunset time from OpenWeatherMap (https://openweathermap.org/)
- Current weather as Icon from OpenWeatherMap
- Current temperature from OpenWeatherMap in °C or °F
- Rain forecast for the current day in mm from OpenWeatherMap
- Current heart rate
- Steps count with filling goal bar
- Climbed floors count with filling goal bar
- Current height in meter or feet
- Several customizable highlight colors

Important:
To get the weather data, your current GPS location is needed. So you have to record
an activity first. After that this location is stored so you just have to repeat it
when you change your location drastically.


References:
Icons: https://icons8.de/

