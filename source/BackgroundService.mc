using Toybox.Background;
using Toybox.System;
using Toybox.Communications;
using Toybox.Application;

(:background)
class BackgroundService extends System.ServiceDelegate {
	
	(:background_method)
	function initialize() {
//		System.println("Background Service - initialize");
		System.ServiceDelegate.initialize();
	}

	// Make web request if property flag NeedWebRequest == true
	(:background_method)
	function onTemporalEvent() {
//		System.println("Background Service - onTemporalEvent");
		var needWebRequest = Application.getApp().getProperty("NeedWebRequest");
//		System.println("NeedWebRequest = " + needWebRequest);
		if (needWebRequest != null && needWebRequest == true) {

			makeWebRequest("https://api.openweathermap.org/data/2.5/onecall",
			{
				"lat" => Application.getApp().getProperty("SavedLocationLat"),
				"lon" => Application.getApp().getProperty("SavedLocationLon"),
				"exclude" => "minutely,hourly",
				"appid" => "fadff1b4220cb4fffeb35b5e9bf1fcc2",
				"units" => "metric"
			},
			method(:onReceiveOpenWeatherMapData)
			);
		} 
	}

	(:background_method)
	function onReceiveOpenWeatherMapData(responseCode, data) {
		var result;
		
//		System.println("Background Service - onReceiveOpenWeatherMapData");
		
		if (responseCode == 200) {
			result = {
				"lat"  				=> data["lat"],
				"lon"  				=> data["lon"],
				"current_dt"   		=> data["current"]["dt"],					// Current Date to check last receive time
				"current_temp" 		=> data["current"]["temp"],
				"current_sunrise"	=> data["current"]["sunrise"],
				"current_sunset"	=> data["current"]["sunset"],
				"current_icon" 		=> data["current"]["weather"][0]["icon"],
				"next_sunrise"		=> data["daily"][1]["sunrise"],
				"today_rain"		=> data["daily"][0]["rain"],
				"day0_dt"			=> data["daily"][0]["dt"],					// Day[0] Date for test purpose
				"day1_dt"			=> data["daily"][1]["dt"]					// Day[1] Date for test purpose
			};

		// HTTP error
		} else {
			result = { "httpError" => responseCode };
		}
		Background.exit({ "OpenWeatherMapData" => result });
	}

	// The one and only actual Web request
	(:background_method)
	function makeWebRequest(url, params, callback) {
//		System.println("Background Service - makeWebRequest");
		
		var options = {
			:method => Communications.HTTP_REQUEST_METHOD_GET,
			:headers => {"Content-Type" => Communications.REQUEST_CONTENT_TYPE_URL_ENCODED},
			:responseType => Communications.HTTP_RESPONSE_CONTENT_TYPE_JSON
		};

		Communications.makeWebRequest(url, params, options, callback);
	}
}
