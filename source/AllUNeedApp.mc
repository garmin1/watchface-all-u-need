using Toybox.Application;
using Toybox.WatchUi;
using Toybox.Time;
using Toybox.Background;

var locationLat = null;
var locationLon = null;

(:background)
class AllUNeedApp extends Application.AppBase {

	var appView;

    function initialize() {
        AppBase.initialize();
    }

    // Return the initial view of your application here
    function getInitialView() {
    	appView = new AllUNeedView();
    	onSettingsChanged();
        return [appView];
    }
    
    // New app settings have been received so trigger a UI update
	function onSettingsChanged() {
		appView.onSettingsChanged();
		WatchUi.requestUpdate();
	}
    
    (:background_method)
	function getServiceDelegate() {
		return [new BackgroundService()];
	}
	
	// Handle data received from BackgroundService.
	(:background_method)
	function onBackgroundData(data) {
//		System.println("App - onBackgroundData - Handle received data");
		var receivedData = data["OpenWeatherMapData"];
//		System.println("App - onBackgroundData - receivedData: " + receivedData);
		
		if (receivedData["httpError"]) {
//			System.println(receivedData);
			System.println("Error retrieving weather data.");
			// needWebRequest is still true so try again
			return;
		}

		// New data received: clear NeedWebRequest flag and overwrite stored data.
		setProperty("NeedWebRequest", false);
		setProperty("OpenWeatherMapData", receivedData);

		WatchUi.requestUpdate();
	}
	
	// Determine if Web request is needed
	(:background_method)
	function checkNeedOfWebRequest() {
		// Location from Activity?
		var location = Activity.getActivityInfo().currentLocation;
		if (location) {
//			System.println("App - checkNeedOfWebRequest - Save new location");
			location = location.toDegrees();
			locationLat = location[0].toFloat();
			locationLon = location[1].toFloat();

			setProperty("SavedLocationLat", locationLat);
			setProperty("SavedLocationLon", locationLon);

		// Location from Activity not available -> take stored value
		} else {
			var lat = getProperty("SavedLocationLat");
			if (lat != null) {
				locationLat = lat;
			}
			var lon = getProperty("SavedLocationLon");
			if (lon != null) {
				locationLon = lon;
			}
		}

//		System.println("NeedWebRequest = " + getProperty("NeedWebRequest"));
		var needWebRequest = getProperty("NeedWebRequest") == null ? false : getProperty("NeedWebRequest");

		// Weather: Location must be available
		if (locationLat != null) {
//			System.println("locationLat is available");
			var openWeatherMapData = Application.getApp().getProperty("OpenWeatherMapData");

			// No existing data.
			if (openWeatherMapData == null) {
//				System.println("No Weather data available! -> Set NeedWebRequest to true");
				needWebRequest = true;
			// Weather data already available: check age and location variance
			} else {
				if(openWeatherMapData.get("current_dt") == null) {
					needWebRequest = true;	// Just to be sure...
				} else {
//					System.println("OWM dt: " + openWeatherMapData.get("current_dt"));
//					System.println("OWM sunrise: " + openWeatherMapData.get("current_sunrise"));
//					System.println("OWM sunrise: " + openWeatherMapData.get("current_sunset"));
					
					var weatherDataAge = openWeatherMapData.get("current_dt").toLong();
					var nextWebRequestTime = weatherDataAge + 1800;	// age + 30 minutes
					
//					System.println("NOW: 		   " + Time.now().value());
//					System.println("Next Request:  " + nextWebRequestTime);
					
					// Existing data is older than 30 mins
					if ((Time.now().value() > nextWebRequestTime) ||
						// Existing data not for this location (0.01° = 1,11 km)
						(((locationLat - openWeatherMapData.get("lat")).abs() > 0.02) || 
							((locationLon - openWeatherMapData.get("lon")).abs() > 0.02))) {
	
						needWebRequest = true;
					} //else {
//						System.println("Data too new or same location -> No Update");
//					}
				}
			}
		}

		if (needWebRequest) {
			var lastTime = Background.getLastTemporalEventTime();
			if (lastTime) {
				var nextTime = lastTime.add(new Time.Duration(300)); // Minimum wait time
				// If data is too old so nextTime is in the past -> triggger immediately
				if(Time.now().value() > nextTime.value()) {
					Background.registerForTemporalEvent(Time.now());
//					System.println("Weather request time = now: " + Time.now().value());
				} else {
					Background.registerForTemporalEvent(nextTime);
//					System.println("Weather request time = next: " + nextTime.value());
				}
			} else {
				// No last time -> do it now
				Background.registerForTemporalEvent(Time.now());
//				System.println("Weather request time (no last time) = now: " + Time.now().value());
			}
		}

		setProperty("NeedWebRequest", needWebRequest);
	}

}