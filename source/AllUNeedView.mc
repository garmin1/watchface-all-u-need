using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;
using Toybox.Lang;
using Toybox.Time.Gregorian as Date;
using Toybox.ActivityMonitor;
using Toybox.Position;
using Toybox.Application;
using Toybox.Math;
using Toybox.Time;

class AllUNeedView extends WatchUi.WatchFace {

	const COLOR_HIGHLIGHT = 12123922;
	var colorsDict = {	0 => 12123922,	// SportyLemon
				  		1 => 1088215,	// ConnectBlue
				  		2 => 16736768,	// BandOrange
				  		3 => 16777215,	// ClassyWhite
				  		4 => 16735838,	// Watermelon
				  		5 => 10331985,	// TacticalGreen
				  		6 => 13853904,  // ModernPurple
				  		7 => 16776960,	// SunnyYellow
				  		8 => 14680064};	// SweetCherry

	var fontCat = null;
	var fontCatLicht = null;
	var iconAlarmClockActive = null;
	var iconAlarmClockInactive = null;
	var iconPhoneConnected = null;
	var iconPhoneNotConnected = null;
	var iconMessageEmpty = null;
	var iconMessageFilled = null;
	var iconSteps = null;
	var iconStepsFilled = null;
	var iconFloors = null;
	var iconFloorsFilled = null;
	var iconHeartPulse = null;
	var iconHeart = null;
	var iconCalories = null;
	var iconCaloriesFilled = null;
	var iconStopWatch = null;
	var iconStopWatchFilled = null;
	var iconDistance = null;
	var weatherIconFont = null;
	var iconArrowUp = null;
	var iconArrowDown = null;
	var iconRain = null;
	var theme = 0;
	var color = COLOR_HIGHLIGHT;
	var caloriesGoal = null;

    function initialize() {
        WatchFace.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
    	fontCat = WatchUi.loadResource(Rez.Fonts.cat);
    	fontCatLicht = WatchUi.loadResource(Rez.Fonts.catLicht);
    	iconAlarmClockActive = WatchUi.loadResource(Rez.Drawables.AlarmClockWhiteIcon);
    	iconAlarmClockInactive = WatchUi.loadResource(Rez.Drawables.AlarmClockGreyIcon);
    	iconPhoneConnected = WatchUi.loadResource(Rez.Drawables.PhoneWhiteIcon);
    	iconPhoneNotConnected = WatchUi.loadResource(Rez.Drawables.PhoneGreyIcon);
    	iconMessageEmpty = WatchUi.loadResource(Rez.Drawables.MessageEmptyIcon);
    	iconMessageFilled = WatchUi.loadResource(Rez.Drawables.MessageFilledIcon);
    	iconSteps = WatchUi.loadResource(Rez.Drawables.StepsIcon);
    	iconStepsFilled = WatchUi.loadResource(Rez.Drawables.StepsFilledIcon);
    	iconFloors = WatchUi.loadResource(Rez.Drawables.FloorsIcon);
    	iconFloorsFilled = WatchUi.loadResource(Rez.Drawables.FloorsFilledIcon);
    	iconHeartPulse = WatchUi.loadResource(Rez.Drawables.HeartPulseIcon);
    	iconHeart = WatchUi.loadResource(Rez.Drawables.HeartIcon);
    	iconCalories = WatchUi.loadResource(Rez.Drawables.CaloriesIcon);
    	iconCaloriesFilled = WatchUi.loadResource(Rez.Drawables.CaloriesFilledIcon);
    	iconStopWatch = WatchUi.loadResource(Rez.Drawables.StopWatchIcon);
    	iconStopWatchFilled = WatchUi.loadResource(Rez.Drawables.StopWatchFilledIcon);
    	iconDistance = WatchUi.loadResource(Rez.Drawables.DistanceIcon);
    	weatherIconFont = WatchUi.loadResource(Rez.Fonts.WeatherIconsFont);
    	iconArrowUp = WatchUi.loadResource(Rez.Drawables.ArrowUpIcon);
    	iconArrowDown = WatchUi.loadResource(Rez.Drawables.ArrowDownIcon);
    	iconRain = WatchUi.loadResource(Rez.Drawables.RainIcon);
        setLayout(Rez.Layouts.WatchFace(dc));
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
    	dc.clear();
    
    	theme = Application.getApp().getProperty("Theme");
    	color = colorsDict.get(theme);
    	caloriesGoal = Application.getApp().getProperty("CaloriesGoal");
    	var df1 = Application.getApp().getProperty("Data1");
    	var df2 = Application.getApp().getProperty("Data2");
    	var df3 = Application.getApp().getProperty("Data3");
    
    	var settings = System.getDeviceSettings();
    	var activityInfo = ActivityMonitor.getInfo();
    
        setHourDisplay(dc);							// Hours
        setMinuteDisplay(dc);						// Minutes
        setDateDisplay(dc);							// Weekday and date
        setAccuDisplay(dc);							// Battery
        setAlarmClockDisplay(dc, settings);			// Alarm set?
        setPhoneConnectedDisplay(dc, settings);		// Phone connected?
        setNotificationDisplay(dc, settings);		// Notification
        
        drawRect(dc);								// Color Rectangle

		drawDependingOnSetting(1, df1, dc, activityInfo, settings, caloriesGoal);	// Data Field 1
		drawDependingOnSetting(2, df2, dc, activityInfo, settings, caloriesGoal);	// Data Field 2
		drawDependingOnSetting(3, df3, dc, activityInfo, settings, caloriesGoal);	// Data Field 3
        
        setAltitudeDisplay(dc);						// Current altitude
        setWeatherDisplay(dc, settings);			// Weather data (Sunrise, sunset, icon, temperature, rain)
		
        // Not sure why this does not work
//        View.onUpdate(dc);
    }

	function drawDependingOnSetting(dataFieldNumber, dataFieldValue, dc, activityInfo, settings, caloriesGoal) {
		var startY = 0;
		
		switch(dataFieldNumber) {
			case 1: startY = 104;
			break;
			case 2: startY = 148;
			break;
			case 3: startY = 190;
			break; 
		}
		
		switch(dataFieldValue) {
			case 0: setPulseDisplay(dc, startY);
			break;
			case 1: setStepsCountDisplay(dc, activityInfo, startY);	
			break;
			case 2: setFloorsCountDisplay(dc, activityInfo, startY);
			break;
			case 3: setCaloriesDisplay(dc, activityInfo, caloriesGoal, startY);
			break;
			case 4: setDistanceDisplay(dc, activityInfo, settings, startY);
			break;
			case 5: setActiveMinutesDisplay(dc, activityInfo, startY);
			break;
		}
	}

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. So update Weather data
    function onExitSleep() {
//    	System.println("onExitSleep -> checkNeedOfWebRequest");
		Application.getApp().checkNeedOfWebRequest();
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
//    	System.println("onEnterSleep -> UI Update");
    	WatchUi.requestUpdate();
    }
    
    // User changed clock settings. So update Weather data
	function onSettingsChanged() {
//		System.println("onSettingsChanged -> checkNeedOfWebRequest");
		Application.getApp().checkNeedOfWebRequest();
	}
    
    function setHourDisplay(dc) {
        var clockTime = System.getClockTime();
        var hour = clockTime.hour;
        
        if(!System.getDeviceSettings().is24Hour) {
        	if(hour > 12) {
        		hour = hour - 12;
        		if(hour == 0) {
        			hour = 12;
        		}
        	}
        }
        
       	var hourString = Lang.format("$1$", [hour.format("%02d")]);
       
	   	dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
       	dc.drawText(62, 26, fontCat, hourString, Graphics.TEXT_JUSTIFY_CENTER);
       
       	dc.setColor(color, Graphics.COLOR_WHITE);
       	dc.fillRectangle(23, 130, 78, 3);
    }

    function setMinuteDisplay(dc) {
        var clockTime = System.getClockTime();
        var minuteString = Lang.format("$1$", [clockTime.min.format("%02d")]);

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(62, 112, fontCatLicht, minuteString, Graphics.TEXT_JUSTIFY_CENTER);
    }
    
    function drawRect(dc) {
    	dc.setColor(color, Graphics.COLOR_WHITE);
    	dc.fillRectangle(131, 0, 95, 260);
    }
    
    function setDateDisplay(dc) {        
		var today = Date.info(Time.now(), Time.FORMAT_SHORT);
		var day = today.day.format("%02d");
		var month = today.month.format("%02d");
		var weekdayResource = today.day_of_week;
		
		var dateString = Lang.format("$1$.$2$.", [day, month]);
		
		switch(today.day_of_week) {
			case 1: weekdayResource = Rez.Strings.Sun;
			break;
			case 2: weekdayResource = Rez.Strings.Mon;
			break;
			case 3: weekdayResource = Rez.Strings.Tue;
			break;
			case 4: weekdayResource = Rez.Strings.Wed;
			break;
			case 5: weekdayResource = Rez.Strings.Thu;
			break;
			case 6: weekdayResource = Rez.Strings.Fri;
			break;
			case 7: weekdayResource = Rez.Strings.Sat;
			break;
		}
		
		dc.setColor(color, Graphics.COLOR_TRANSPARENT);
		
		// Manually draw calendar icon to use different colors easily
		var startX = 79;
		var startY = 14;
		dc.drawLine(startX + 1, startY, startX + 13, startY); 			// up left to up right
		dc.drawLine(startX, startY + 1, startX, startY + 12);			// up left to down left
		dc.drawLine(startX + 1, startY + 12, startX + 13, startY + 12);	// down left to down right
		dc.drawLine(startX + 13, startY + 11, startX + 13, startY);		// down right to up right
		dc.drawLine(startX + 1, startY + 4, startX + 13, startY + 4);	// line between
		dc.drawLine(startX + 3, startY - 1, startX + 3, startY + 2);	// left hook
		dc.drawLine(startX + 10, startY - 1, startX + 10, startY + 2);	// right hook
		dc.drawPoint(startX + 4, startY + 6);							// line 1
		dc.drawPoint(startX + 6, startY + 6);							// line 1
		dc.drawPoint(startX + 8, startY + 6);							// line 1
		dc.drawPoint(startX + 10, startY + 6);							// line 1
		dc.drawPoint(startX + 2, startY + 8);							// line 2
		dc.drawPoint(startX + 4, startY + 8);							// line 2
		dc.drawPoint(startX + 6, startY + 8);							// line 2
		dc.drawPoint(startX + 8, startY + 8);							// line 2
		dc.drawPoint(startX + 10, startY + 8);							// line 2
		dc.drawPoint(startX + 2, startY + 10);							// line 3
		dc.drawPoint(startX + 4, startY + 10);							// line 3
		dc.drawPoint(startX + 6, startY + 10);							// line 3
		
		dc.drawText(97, 11, Graphics.FONT_XTINY, WatchUi.loadResource(weekdayResource), Graphics.TEXT_JUSTIFY_LEFT);
		dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(119, 35, Graphics.FONT_XTINY, dateString, Graphics.TEXT_JUSTIFY_VCENTER);
    }
    
    function setAccuDisplay(dc) {
    	var accu = System.getSystemStats().battery;
    	var accuString = accu.format("%d"); 
    	dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
    	
    	var barLength = 12.0;
    	var fillStartX = 234;
    	var fillStartY = 124;
    	var width = 16;
    	var height = 8;
	    	
    	var length = (barLength / 100) * accu;
    	
    	dc.drawRectangle(fillStartX, fillStartY, width, height);
    	dc.fillRectangle(fillStartX + 2, fillStartY + 2, Math.round(length), 4);
    	dc.drawRectangle(fillStartX + width, fillStartY + 2, 2, 4);
    	
        dc.drawText(242, 131, Graphics.FONT_XTINY, accuString, Graphics.TEXT_JUSTIFY_CENTER);
    }
    
    function setNotificationDisplay(dc, settings) {
		var count = settings.notificationCount;
		
		if(count <= 0) {
			dc.drawBitmap(86, 222, iconMessageEmpty);
		} else {
			dc.drawBitmap(86, 222, iconMessageFilled);
			dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
			dc.fillCircle(113, 244, 8);
			dc.setColor(color, Graphics.COLOR_TRANSPARENT);
			
			if(count < 10) {
				dc.drawText(108, 234, Graphics.FONT_XTINY, count, Graphics.TEXT_JUSTIFY_LEFT);
			} else {
				count = "9+";
				dc.drawText(108, 234, Graphics.FONT_XTINY, count, Graphics.TEXT_JUSTIFY_LEFT);
			}
		}
    }
    
    function setAlarmClockDisplay(dc, settings) {
    	var isAlarmSet = settings.alarmCount > 0 ? true : false;
    	
    	if(isAlarmSet) {
    		dc.drawBitmap(235, 160, iconAlarmClockActive);	
    	} else {
    		dc.drawBitmap(235, 160, iconAlarmClockInactive);	
    	}
    }
    
    function setPhoneConnectedDisplay(dc, settings) {
    	if(settings.phoneConnected) {
    		dc.drawBitmap(235, 97, iconPhoneConnected);
    	} else {
    		dc.drawBitmap(235, 97, iconPhoneNotConnected);
    	}
    }
    
    function setStepsCountDisplay(dc, activityInfo, startY) {
    	var countSteps = activityInfo.steps;
    	var countStepGoal = activityInfo.stepGoal;

		if(countSteps >= countStepGoal) {
			dc.drawBitmap(131, startY - 3, iconStepsFilled);
		} else {
			dc.drawBitmap(131, startY - 3, iconSteps);
		}

    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	dc.drawText(214, startY, Graphics.FONT_TINY, countSteps, Graphics.TEXT_JUSTIFY_RIGHT);
    	
    	if(countStepGoal != null && countStepGoal != 0) {
	    	var stepGoalReached = countSteps.toFloat() / countStepGoal.toFloat();
	    	var barLength = 73;
	    	var fillStartX = 142;
	    	var fillStartY = startY + 26;
	    	
	    	var length = barLength * stepGoalReached;
	    	length = length > barLength ? barLength : length;
	    	dc.drawRectangle(fillStartX, fillStartY, barLength, 6);
	    	dc.fillRectangle(fillStartX, fillStartY, length, 6);
    	}
    }
    
    function setFloorsCountDisplay(dc, activityInfo, startY) {
    	var countFloors = activityInfo.floorsClimbed;
    	var countFloorsGoal = activityInfo.floorsClimbedGoal;
    	
    	if(countFloors >= countFloorsGoal) {
    		dc.drawBitmap(137, startY - 1, iconFloorsFilled);
    	} else {
    		dc.drawBitmap(137, startY - 1, iconFloors);
    	}
    	
    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	dc.drawText(214, startY, Graphics.FONT_TINY, countFloors, Graphics.TEXT_JUSTIFY_RIGHT);
    	
    	if(countFloorsGoal != null && countFloorsGoal != 0) {
	    	var floorGoalReached = countFloors.toFloat() / countFloorsGoal.toFloat();
	    	var barLength = 73;
	    	var fillStartX = 142;
	    	var fillStartY = startY + 26;
	    	
	    	var length = barLength * floorGoalReached;
	    	length = length > barLength ? barLength : length;
	    	dc.drawRectangle(fillStartX, fillStartY, barLength, 6);
	    	dc.fillRectangle(fillStartX, fillStartY, length, 6);
    	}
    }
    
    function setAltitudeDisplay(dc) {
    	var altitude = Activity.getActivityInfo().altitude;
    	var unit = "";
    	
    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	
    	if(altitude != null) {
    		if(System.getDeviceSettings().elevationUnits == System.UNIT_METRIC) {
    			altitude = altitude.toFloat().format("%.0d");
    			unit = "m";
    		} else {
    			altitude = (altitude.toFloat() * 3.2808399).format("%.0d");
    			unit = "ft";
    		}
    		dc.drawText(141, 228, Graphics.FONT_XTINY, altitude + unit, Graphics.TEXT_JUSTIFY_LEFT);
    	} else {
    		dc.drawText(141, 228, Graphics.FONT_XTINY, "--", Graphics.TEXT_JUSTIFY_LEFT);
    	}
    }
    
    function setPulseDisplay(dc, startY) {
		var heartRate = null;
		var previous = null;
		
		if (ActivityMonitor has :getHeartRateHistory) {
			heartRate = Activity.getActivityInfo().currentHeartRate;

			if(heartRate == null) {
				var heartRateHistory = ActivityMonitor.getHeartRateHistory(1, true);
				previous = heartRateHistory.next();
				
				if(previous !=null && previous.heartRate != ActivityMonitor.INVALID_HR_SAMPLE) {
					heartRate = previous.heartRate;
				}
			}
		}

		if(heartRate != null) {
			dc.drawBitmap(137, startY, iconHeartPulse);
		} else {
			dc.drawBitmap(137, startY, iconHeart);
			heartRate = "--";
		}

    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	dc.drawText(214, startY, Graphics.FONT_SYSTEM_TINY, heartRate, Graphics.TEXT_JUSTIFY_RIGHT);
    }
    
    function setCaloriesDisplay(dc, activityInfo, caloriesGoal, startY) {
    	var calories = activityInfo.calories;
    	
		if(caloriesGoal != null && calories >= caloriesGoal.toNumber()) {
			dc.drawBitmap(137, startY + 3, iconCaloriesFilled);
		} else {
			dc.drawBitmap(137, startY + 3, iconCalories);
		}

    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	dc.drawText(214, startY, Graphics.FONT_SYSTEM_TINY, calories, Graphics.TEXT_JUSTIFY_RIGHT);
    	
    	if(caloriesGoal != null && caloriesGoal > 0) {
	    	var goalReached = calories.toFloat() / caloriesGoal.toFloat();
	    	var barLength = 73;
	    	var fillStartX = 142;
	    	var fillStartY = startY + 26;
	    	
	    	var length = barLength * goalReached;
	    	length = length > barLength ? barLength : length;
	    	dc.drawRectangle(fillStartX, fillStartY, barLength, 6);
	    	dc.fillRectangle(fillStartX, fillStartY, length, 6);
    	}
    }
    
    function setActiveMinutesDisplay(dc, activityInfo, startY) {
    	var activeMinutesWeek = activityInfo.activeMinutesWeek.total;
    	var activeMinutesGoal = activityInfo.activeMinutesWeekGoal;

		if(activeMinutesWeek >= activeMinutesGoal) {
			dc.drawBitmap(133, startY - 2, iconStopWatchFilled);
		} else {
			dc.drawBitmap(137, startY - 2, iconStopWatch);
		}

    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	dc.drawText(214, startY, Graphics.FONT_TINY, activeMinutesWeek, Graphics.TEXT_JUSTIFY_RIGHT);
    	
    	if(activeMinutesGoal != null && activeMinutesGoal != 0) {
	    	var goalReached = activeMinutesWeek.toFloat() / activeMinutesGoal.toFloat();
	    	var barLength = 73;
	    	var fillStartX = 142;
	    	var fillStartY = startY + 26;
	    	
	    	var length = barLength * goalReached;
	    	length = length > barLength ? barLength : length;
	    	dc.drawRectangle(fillStartX, fillStartY, barLength, 6);
	    	dc.fillRectangle(fillStartX, fillStartY, length, 6);
    	}
    }
    
    function setDistanceDisplay(dc, activityInfo, settings, startY) {
    	var distance = activityInfo.distance.toFloat();	// cm

    	if(settings.distanceUnits == System.UNIT_METRIC) {
    		distance = distance / 100000.0;	// km
    	} else {
    		distance = distance * 0.0000062137;	// miles
    	}
    	
    	dc.drawBitmap(137, startY - 1, iconDistance);

    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	if(distance >= 100) {
    		dc.drawText(214, startY, Graphics.FONT_TINY, distance.format("%.0f"), Graphics.TEXT_JUSTIFY_RIGHT);
    	} else {
    		dc.drawText(214, startY, Graphics.FONT_TINY, distance.format("%.1f"), Graphics.TEXT_JUSTIFY_RIGHT);
    	}	
    }
    
    function setWeatherDisplay(dc, settings) {
    	var openWeatherMapData = Application.getApp().getProperty("OpenWeatherMapData");
    	
    	if(openWeatherMapData == null) {
//    		System.println("View: No Weather data");
    		return;
    	}
    	
    	var icon = {
					// Day icon   Night icon    Description
					"01d" => "H", "01n" => "f", // clear sky
					"02d" => "G", "02n" => "g", // few clouds
					"03d" => "B", "03n" => "h", // scattered clouds
					"04d" => "I", "04n" => "I", // broken clouds: day and night use same icon
					"09d" => "E", "09n" => "d", // shower rain
					"10d" => "D", "10n" => "c", // rain
					"11d" => "C", "11n" => "b", // thunderstorm
					"13d" => "F", "13n" => "e", // snow
					"50d" => "A", "50n" => "a", // mist
		};
    	
    	var current_sunrise = new Time.Moment(openWeatherMapData.get("current_sunrise"));
    	var current_sunset = new Time.Moment(openWeatherMapData.get("current_sunset"));
    	var next_sunrise = new Time.Moment(openWeatherMapData.get("next_sunrise"));
    	var sunrise = Date.info(current_sunrise, Time.FORMAT_SHORT);
    	var sunset = Date.info(current_sunset, Time.FORMAT_SHORT);
    	var sunrise2 = Date.info(next_sunrise, Time.FORMAT_SHORT);
    	var now = Date.info(Time.now(), Time.FORMAT_SHORT);

		var sunriseTimeSec = (sunrise.hour * 3600) + (sunrise.min * 60) + sunrise.sec;
		var sunsetTimeSec = (sunset.hour * 3600) + (sunset.min * 60) + sunset.sec;
		var nowTimeSec = (now.hour * 3600) + (now.min * 60) + now.sec;
		
//		System.println("Sunrise Time: " + sunriseTimeSec + " Seconds");
//		System.println("Sunset Time : " + sunsetTimeSec + " Seconds");
//		System.println("Now Time    : " + nowTimeSec + " Seconds");

    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	
    	// During the day
    	if(nowTimeSec > sunriseTimeSec && nowTimeSec < sunsetTimeSec) {
//    		System.println("Now between sunrise and sunset");
    		dc.drawBitmap(137, 17, iconArrowDown);
    		
    		if(settings.is24Hour) {
    			var timeString = sunset.hour.format("%02d") + ":" + sunset.min.format("%02d");
    			dc.drawText(190, 15, Graphics.FONT_SYSTEM_XTINY, timeString, Graphics.TEXT_JUSTIFY_RIGHT);
    		} else {
    			var hour = sunset.hour > 12 ? sunset.hour - 12 : sunset.hour;
    			// Quite sure this can't happen...
    			if(hour == 0) {
        			hour = 12;
        		}
    			hour = hour.format("%02d");
    			var timeString = hour + ":" + sunset.min.format("%02d");
    			dc.drawText(190, 15, Graphics.FONT_SYSTEM_XTINY, timeString, Graphics.TEXT_JUSTIFY_RIGHT);
    		}
    	// Before sunrise or after sunset
    	} else {
    		dc.drawBitmap(137, 18, iconArrowUp);
    		var sunrise_actual = sunrise;
    		
    		if(nowTimeSec > sunsetTimeSec) {
    			// Use sunrise from next day
    			sunrise_actual = sunrise2;
    		}
    		
    		if(settings.is24Hour) {
    			var timeString = sunrise_actual.hour.format("%02d") + ":" + sunrise_actual.min.format("%02d");
    			dc.drawText(190, 15, Graphics.FONT_SYSTEM_XTINY, timeString, Graphics.TEXT_JUSTIFY_RIGHT);
    		} else {
    			var hour = sunrise_actual.hour > 12 ? sunrise_actual.hour - 12 : sunrise_actual.hour;
    			// Quite sure this can't happen...
    			if(hour == 0) {
        			hour = 12;
        		}
    			hour = hour.format("%02d");
    			var timeString = hour + ":" + sunrise_actual.min.format("%02d");
    			dc.drawText(190, 15, Graphics.FONT_SYSTEM_XTINY, timeString, Graphics.TEXT_JUSTIFY_RIGHT);
    		}
    	}
    	
    	
    	var current_temp = openWeatherMapData.get("current_temp");
    	
    	if(settings.temperatureUnits == System.UNIT_METRIC) {
    		current_temp = Math.round(current_temp);
    	} else {
    		current_temp = (current_temp * 1.8) + 32;
    	}
    	
    	current_temp = Math.round(current_temp).format("%2d");
    	
    	dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_TRANSPARENT);
    	dc.drawText(217, 40, Graphics.FONT_SYSTEM_TINY, current_temp + "°", Graphics.TEXT_JUSTIFY_RIGHT);
    	
    	var current_icon = openWeatherMapData.get("current_icon");
    	dc.drawText(140, 39, weatherIconFont, icon[current_icon], Graphics.TEXT_JUSTIFY_LEFT);
    	
    	var today_rain = openWeatherMapData.get("today_rain");
    	
    	dc.drawBitmap(155, 74, iconRain);
    	
    	if(today_rain != null) {
    		var rainFormatted = Math.round(today_rain).toNumber();
    		if(rainFormatted == 0) {
    			rainFormatted = "< 1";
    		}
    		dc.drawText(214, 72, Graphics.FONT_SYSTEM_XTINY, rainFormatted + "mm", Graphics.TEXT_JUSTIFY_RIGHT);
    	} else {
    		dc.drawText(214, 72, Graphics.FONT_SYSTEM_XTINY, WatchUi.loadResource(Rez.Strings.No), 
    			Graphics.TEXT_JUSTIFY_RIGHT);
    	}

    }

}
